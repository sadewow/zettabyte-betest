function getInParallel(apiCalls) {
    const promise1 = Promise.resolve("First API call!");
    const promise2 = new Promise((resolve, reject) => {
        setTimeout(resolve, 100, "Second API call!");
    });

    Promise.all([promise1, promise2]).then((values) => {
    console.log(values);
    });
}

let promise = getInParallel([() => Promise.resolve("First API call!"),
() => Promise.resolve("Second API call!")]);

if(promise) {promise
    .then((result) => console.log(result))
    .catch((err) => console.log(err));
}

module.exports.getInParallel = getInParallel;