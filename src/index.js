const express = require('express')

const app = express()
const port = 3000

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb+srv://sadewa:2VRi589gGFqsgnaq@cluster0.3yy7p.mongodb.net/zettabyte?retryWrites=true&w=majority';

// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log('connected to DB')
  

  // Create Collection
  var dbo = db.db("zettabyte");
  dbo.createCollection("Zettabyte Blog", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });

  // Create New Blog
  var blog1 = {
    blogName: "Scooby",
    blogDescription: 'Doo',
    blogDate: '3rd February 2021',
  };
  dbo.collection("Zettabyte Blog").insertOne(blog1, function(err, res) {
    if (err) throw err;
    console.log("1 blog inserted");
    db.close();
  });

  // Find All Blog
  dbo.collection("Zettabyte Blog").find({}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });

  // Find One Blog
  dbo.collection("Zettabyte Blog").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });

  // Update Existing Blog
  var newBlogName = {
    blogName: "Dora",
    blogDescription: 'Doo',
    blogDate: '3rd February 2021',
  };
  dbo.collection("Zettabyte Blog").updateOne(blog1, newBlogName, function(err, res) {
    if (err) throw err;
    console.log("1 document updated");
    db.close();
  });

  // Delete Blog
  var blog2 = {
    blogName: "Winnie",
    blogDescription: 'The Pooh',
    blogDate: '2nd February 2021',
  };
  dbo.collection("Zettabyte Blog").deleteOne(blog2, function(err, obj) {
    if (err) throw err;
    console.log("1 blog deleted");
    db.close();
  });

});

app.get("/", (req, res) => {
    res.send('Hello World')
})

app.listen(port, () => {
    console.log(`app running now at http://localhost:${port}`)
})